import axios from 'axios';
import config from '../config';
const { host } = config;
axios.interceptors.request.use((config) => {
    config.url = host + config.url

    return config;
},(error) => {
    return Promise.reject(error)
})

axios.interceptors.response.use(function(response) {
    return response.data;
}, function(error) {
    return Promise.reject(error)
})
