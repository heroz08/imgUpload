import axios from 'axios';
import {observable, action}  from 'mobx';
import history from '@plugins/history'
export default class Store {
  constructor(){
    this.uploadPorps.beforeUpload = this.beforUpload;
    this.uploadPorps.onRemove = this.remove;
  }

  @observable refreshData = 0;

  fileList = [];
  preImgList = [];

  uploadPorps = {
    beforeUpload: null,
    listType: "picture-card",
    multiple: true,
    accept: ".JPG, .PNG, .GIF, .JEPG, .MP4",
    onRemove: null,
  }

  goback = () => {
    history.push('/')
  }

  beforUpload = (file) => {
    this.fileList = [...this.fileList, file];
    return false;
  }

  remove = (file) => {
    const index = this.fileList.indexOf(file.originFileObj);
    const tempList = this.fileList.slice();
    tempList.splice(index, 1);
    this.setData({
      fileList: tempList
    })
  }

  upload = async() => {
    const formData = new FormData();
    this.fileList.forEach((file, index) => {
      formData.append('file' + (index || ''), file)
    })

    const res = await axios.post('/upload', formData, );
    const {data = []} = res;
    const _data = data.map(item => {
      const obj = {url: item.url, type: 'img'};
      const suf = item.url.split('/').pop().split('.')[1];
      if(suf === 'mp4') {
        obj.type = 'video';
      }
      return obj;
    })
    this.setData({
      preImgList: _data
    })
  }  

  @action setData = (data) => {
    Object.keys(data).forEach(key => {
      this[key] = data[key];
    })
    this.refreshData = this.refreshData + 1
  }
}
