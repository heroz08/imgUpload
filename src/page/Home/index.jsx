import React, { Component } from 'react';
import { Button, Card, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons'
import { observer } from 'mobx-react';
import Store from './store';
import './index.less';


@observer export default class Home extends Component {
  constructor(props) {
    super(props);
    this.store = new Store()
  }

  copyLink = (url) => {
    
    const input = document.createElement('input')
    document.body.appendChild(input)
    input.setAttribute('value', url)
    input.select();
    if (document.execCommand('copy')) {
      document.execCommand('copy');
      message.success('复制成功')
    }
    document.body.removeChild(input)
  }

  renderImage = (images) => {
    return (<div className="images-wrap">
      {
        images.map(item => {
          return (
            <div className="images">
              {
                item.type === 'video' ? (
                <video controls>
                  <source src={item.src} type="video/mp4" />
                </video>
                ) : <img src={item.src} alt="" />
              }
              <div className="copy-wrap">
                <Button onClick={e => this.copyLink(item.src)} type="primary">复制链接</Button>
              </div>
            </div>
          )
        })
      }
    </div>)
  }

  render() {
    const { toUpload, images } = this.store;
    return (
      <div className="home-wrap" freshData={this.store.refreshData}>
        <div className="header">
          <div className="title">图片列表</div>
          <div className="upload">
            <Button type="primary" onClick={toUpload}>上传图片</Button>
          </div>
        </div>
        <Card title="所有图片" headStyle={{ "textAlign": "left" }}>
          {
            images && images.length ? this.renderImage(images) : (
              <p>无图片，去上传吧！</p>
            )
          }
        </Card>
      </div>
    );
  }
}
