import axios from 'axios';
import {observable, action}  from 'mobx';
import history from '@plugins/history';
import config from '../../config';
const {host} = config;
const videoMap = ['mp4']
export default class Store {
  constructor(){
    this.fetchImages()
  }

  @observable refreshData = 0;

  @observable.ref images = [];

  toUpload = () => {
    history.push('/upload')
  }

  fetchImages = () => {
    axios.get('/getImages', {})
    .then(resp => {
      if (resp.code === 0) {
        const data = resp.data.map(image => {
          const obj = { type: 'img' };
          const suffix = image.split('.')[1];
          if(videoMap.includes(suffix)) {
            obj.type = 'video';
          }
          const src = `${host}/uploads/${image}`;
          obj.src = src;
          return obj;
        })
        this.setData({images: data})
      } else {
        message.error(resp.message)
      }
    })
    .catch(error => {
      console.log(error)
    })
  }

  @action setData = (data) => {
    Object.keys(data).forEach(key => {
      this[key] = data[key];
    })
    this.refreshData = this.refreshData + 1
  }
}
