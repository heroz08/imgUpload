import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '@src/page/Home'
import Upload from '@src/page/Upload'

export default class Page extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/upload' component={Upload}/>
        </Switch>
      </div>
    );
  }
}